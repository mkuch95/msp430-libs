#include <msp430x14x.h>
#include "wdt.h"
#include "lcd.h"
#include "BDB.h"


void main( void )
{
    wdtInit();                                    //inicjalizacja wdt
    
    initButtons();                                //inicjalizacja przycisk�w

    initLCD();                                    // inicjalizacja LCD
    clearDisplay();                               // czyszczenie LCD
    
    
    char myChar[8][5] = {
        {0,0,0,0,0},
        {0,0,0,0,0},
        {0,1,0,1,0},
        {0,0,0,0,0},
        {1,0,0,0,1},
        {0,1,1,1,0},
        {0,0,0,0,0},
        {0,0,0,0,0}
    };
    makeDefinedChar(myChar, DCH0);                  //tworzymy i wysy�amy na ekran w�asny znak
    
    
    sendString("Hello! ");
    sendChar(DCH0);
    
    while(1){
      
      if(checkButtonPressedTime(B1)){
          clearDisplay();
          sendString("test liczb:");
          setCursorPosition(4, 1);
          sendInt(1285);
      }
      
      if(checkButtonPressedTime(B2)){
          clearDisplay();
          sendString("inny test:");
          setCursorPosition(4, 1);
          sendInt(-1285);
      }

    }
}
