
#include "io430.h"

#include "BDB.h"

int main( void )
{
  // Stop watchdog timer to prevent time out reset
  WDTCTL = WDTPW + WDTHOLD;

  //inicjujemy naszą bibliotekę
  initAllBDB();

  while(1){
    
    //włączamy diodę REL1 poprzez wciśnięcie przycisku B1 na 0.5 sekundy, wyłączyć ją możemy też w taki sposób
    if(checkButtonPressedTime(B1) > 500){
      turnOnRel1();
      
      while(checkButtonPressedTime(B1) < 500);
      turnOff(REL1);
    }
    
	//odgrywanie przykładowych dźwięków
    if(checkButtonPressed(B2)){
      playSound(LOW, 1000);
      playSound(HIGH, 1000);
      playSound(MEDIUM, 1000);
    }
    
    
  }
}
