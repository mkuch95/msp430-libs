

typedef enum speeds
{
    S300 = 300,
    S600 = 600,
    S1200 = 1200,
    S2400 = 2400,
    S4800 = 4800,
    S9600 = 9600,
    S14400 = 14400,
    S19200 = 19200
} Speeds;


typedef enum dataLength
{
    BITS7,
    BITS8
} DataLength;


typedef enum parity
{
    PNone, //wy��czone
    PEven, //parzysty
    POdd //nieparzysty
} Parity;


typedef enum stopBits
{
    SB1,
    SB2
} StopBits;




void uartInit(Speeds speed, DataLength dataLength, Parity parity, StopBits stopBits);

void uartCharTransmit(char znak);
void uartStringTransmit(char *str);
char uartDataAvailable();

char uartReadChar();
