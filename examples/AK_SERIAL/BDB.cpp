#include <io430.h>
#include "BDB.h"

//funkcja odczekuj�ca, czas w milisekundach podajemy w argumencie
void _delayms(int time){
  int k;
  while(time--){
	k=260;//240
	while(k--);
  }
}

//inicjalizuje piny przyporz�dkowane do diody STATUS oraz przeka�nik�w
inline void initDevices(){
	// wyj�cie dla status led
	P2DIR |= BIT1;

	// wyj�cie dla przeka�nik�w
	P1DIR |= BIT5 | BIT6;
	P1OUT &= ~(BIT5 | BIT6);
}

//inicjalizacja wyj�� dla buzzera
inline void initBuzzer(){
	P4DIR |= BIT2 | BIT3;
}

//inicjalizacja pin�w jako wej�cia na przyciski
inline void initButtons(){
	P4DIR &= ~(BIT4 | BIT5 | BIT6 | BIT7); //00001111
}

//inicjalizacja wszystkich funkcji biblioteki
void initAllBDB(){
    initDevices();
    initBuzzer();
    initButtons();	
}

//odgrywanie d�wi�ku przez buzzer, w argumentach podajemy ton(niski, �redni, wysoki) oraz czas trwania d�wi�ku w milisekundach
void playSound(Sounds sound, int time){
  int minus = sound / 130;
  
  int k;
  while(time-=minus){
	P4OUT |= BIT2;
	
	k=sound;
	while(k--);
	
	P4OUT &= ~BIT2;
	
	k=sound;
	while(k--);
        
  }
}

//funkcja sprawdza czy przycisk jest wci�ni�ty, je�eli tak oczekuje na jego zwolnienie i zwraca warto�� jak d�ugo zosta� wci�ni�ty, w przeciwnym wypadku zwraca warto�� 0
int checkButtonPressedTime(Buttons button){
  int time = 0;
  
  
  while(checkButtonPressed(button)){
	time++;
	_delayms(1);
  }
  
  return time;
}

//funkcja zwraca warto�� logiczn� informuj�c� czy przycisk podany jako argument jest wci�ni�ty
int checkButtonPressed(Buttons button){
	switch(button){
		case B1: 
			return ((P4IN & BIT4) == 0);
		case B2:
			return ((P4IN & BIT5) == 0);
		case B3: 
			return ((P4IN & BIT6) == 0);
		case B4: 
			return ((P4IN & BIT7) == 0);
	}
        return 0;
}

//w��cza jedn� z diod/przeka�nik�w
void turnOn(Devices device){
	if(device == STATUS)
		turnOnStatus();
	else if(device == REL1)
		turnOnRel1();
	else if(device == REL2)
		turnOnRel2();
}

//funkcje skr�cone, bez argument�w
inline void turnOnStatus(){
	P2OUT &= ~(BIT1);
}

inline void turnOnRel1(){
	P1OUT |= (BIT5);
}

inline void turnOnRel2(){
	P1OUT |= (BIT6);
}

//wy��cza jedn� z diod/przeka�nik�w
void turnOff(Devices device){
	if(device == STATUS)
		turnOffStatus();
	else if(device == REL1)
		turnOffRel1();
	else if(device == REL2)
		turnOffRel2();
}

//funkcje skr�cone, bez argument�w
inline void turnOffStatus(){
	P2OUT |= (BIT1);
}

inline void turnOffRel1(){
	P1OUT &= ~(BIT5);
}

inline void turnOffRel2(){
  	P1OUT &= ~(BIT6);
}
