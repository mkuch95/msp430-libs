#include <msp430x14x.h>
#include "interrupts.h"
#include "wdt.h"
#include "BDB.h"
#include "uart.h"


int main(void)
{
  wdtInit();
  
  initAllBDB();
  
  uartInit(S2400, BITS8, PNone, SB1);                        // inicjalizacja UARTa pr�dko�� transmisji 2400
  
  enableInterrupts();
  
  while(1){
    char ch = uartReadChar();
    
    if(ch == 'q'){
      turnOnRel1();
      uartStringTransmit("Wlaczono przekaznik1\r\n");
    }
    if(ch == 'w'){
      turnOffRel1();
      uartStringTransmit("Wylaczono przekaznik1\r\n");
    }
    
    
    if(checkButtonPressed(B1))
        uartStringTransmit("Przycisk1 wcisniety\r\n");
  }
}
