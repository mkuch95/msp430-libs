#include <msp430x14x.h>
#include "uart.h"



char bufor[30];                         // bufor odczytywanych danych
int low = 0;                              // znacznik pocz�teku danych w buforze
int high = 0;                             // zmacznik ko�ca danych w buforze



#pragma vector=UART0RX_VECTOR           // procedura obs�ugi przerwania UART
__interrupt void usart0_rx(void)
{
    bufor[high] = RXBUF0;                     // wpisanie odebranych danych do bufora
    high = (++high) % 30;                     // inkrementowanie znacznika ko�ca danych
}



//doda� opcj� bitu parzysto sci, ilosc bitow danych, parzystosc
void uartInit(Speeds speed, DataLength dataLength, Parity parity, StopBits stopBits)
{
    //konfigurowanie port�w
    //Tx 0x10   P3.4
    //Rx 0x20   P3.5
    P3SEL |= 0x10 + 0x20;
    P3DIR |= 0x10;
    P3DIR &= ~ 0x20;
    //konfigurowanie port�w
    
    int i;
    BCSCTL1 |= XTS;                       // ACLK = LFXT1 = HF XTAL 8MHz
    do
    {
        IFG1 &= ~OFIFG;                       // Czyszczenie flgi OSCFault
        for (i = 0xFF; i > 0; i--);           // odczekanie
    }
    while ((IFG1 & OFIFG));               // dop�ki OSCFault jest ci�gle ustawiona
        BCSCTL2 |= SELM1+SELM0 ;              // MCLK =LFXT1
    
    
    ME1 |= UTXE0 + URXE0;                 // W��czenie USART0 TXD/RXD
    
    if(dataLength == BITS8)                  //d�ugo�� danych w transmisji
        UCTL0 |= CHAR;                        // 8-bit�w
    
    
    if(parity == PEven)                    //w��czenie bit�w parzystosci
        UCTL0 |= PENA | PEV;
    if(parity == POdd)
        UCTL0 |= PENA;
    
    if(stopBits == SB2)                     //bity stopu, domy�lnie jest 1
        UCTL0 |= SPB;
    
    UTCTL0 |= SSEL0;                      // UCLK = ACLK
    
    long ubr = 8000000 / speed;
    UBR00 = ubr;                          // 8MHz/Speed in Bauds
    UBR10 = (ubr >> 8);                   //
    UMCTL0 = 0x5B;                        // Modulation
    UCTL0 &= ~SWRST;                      // Inicjalizacja UARTA
    IE1 |= URXIE0;                        // W��czenie przerwa� od RX
}

//---------------- wysy�anie znaku ---------
void uartCharTransmit(char znak)
{
    while ((IFG1 & UTXIFG0)==0);          // gdy uk��d nie jest zaj�ty
        TXBUF0 = znak;                           // wysy�amy znak
}

//---------------- wysy�anie napisu ----------
void uartStringTransmit(char *str)
{
    while(*str)
        uartCharTransmit(*(str++));
}


//sprawdzanie czy jakie� dane zosta�y wys�ane do mikrokontrolera
inline char uartDataAvailable(){
    return (high != low);
}


//czytanie znaku z bufora
char uartReadChar(){
    if(!uartDataAvailable()) return 0;
    
    char result = bufor[low];                 // odczyt znaku z bufora
    low = (++low) % 30;                     // inkrementowanie znaczika pocz�tka danych
    
    return result;
}
