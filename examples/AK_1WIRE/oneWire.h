
void oneWireInit();
void oneWireWriteByte(char byte);
char oneWireReadByte();
char oneWireReset();
void oneWireSearchReset();
char oneWireSearch(unsigned char *newAddr);
