#include <msp430x14x.h>
#include "delays.h"


//odczekanie zadanego czasu w mikrosekundach
void delayus(unsigned int time){
    for(int k = 0; k != time; ++k){
        _NOP();
        _NOP();
        _NOP();
        _NOP();
    }
}


//odczekanie zadanego czasu w milisekundach
void delayms(unsigned char time){
    for(int j = 0; j != time; ++j){
        delayus(1000);
    }
}
