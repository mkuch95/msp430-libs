
//oznaczenie znaków zdefiniowanych przez użytkownika
typedef enum definedChars
{
    DCH0 = 0,
    DCH1 = 1,
    DCH2 = 2,
    DCH3 = 3,
    DCH4 = 4,
    DCH5 = 5,
    DCH6 = 6,
    DCH7 = 7
} DefinedChars;


//komendy wysyłane do wyświetlacza
typedef enum commands{
    CLR_DISP =		0x01,	    	// clear display
    CUR_HOME =		0x02,	        // return home
    ENTRY_INC =		0x06,           // entry mode increment
    ENTRY_INC_ROL =	0x07,           // entry mode increment with rol data
    ENTRY_DEC =		0x04,           // entry mode decrement
    ENTRY_DEC_ROL =	0x05,           // entry mode decrement witch rol data
    DISP_OFF =		0x08,	        // all display off
    DISP_ON =		0x0c,	        // all display on

    DATA_ROL_LEFT =	0x18,           // rol data left
    DATA_ROL_RIGHT =    0x1c,           // rol data right
    CUR_SHIFT_LEFT =    0x10,           // shift coursor left
    CUR_SHIFT_RIGHT =   0x14,           // shift coursor right

    DD_RAM_ADDR =	0x80,	        // 0x0 position
    DD_RAM_ADDR2 =	0xc0,	        // 0x1 position
    DD_RAM_ADDR3 =	0x28,	        //
    CG_RAM_ADDR =	0x40	        // set CG_RAM
} Commands;


void initLCD(void);
void clearDisplay();
void gotoFirstLine();
void gotoSecondLine();
void sendChar(unsigned char c);
void sendCmd (unsigned char c);
void sendString(char *str);
void sendInt(int number);
void sendFloat(float number, char precision);
void makeDefinedChar(char c[][5], DefinedChars charId);
void setCursorPosition(unsigned char x, unsigned char y);
