#include <msp430x14x.h>
#include "wdt.h"
#include "bdb.h"
#include "oneWire.h"
#include "lcd.h"


int main(){
    wdtInit();
    
    initButtons();
    
    initLCD();
    clearDisplay();
	
	
    oneWireInit();
    
	while(1){
                if(checkButtonPressedTime(B2)){
                    clearDisplay();
                    if(oneWireReset())
			sendString("czujnik podlaczony");
		    else
			sendString("brak czujnika");
                }
                if(checkButtonPressedTime(B3)){
                    clearDisplay();
                    
                    oneWireSearchReset();
                    
                    unsigned char address[8] = {0, 0, 0, 0, 0, 0, 0, 0};
                    
                    if(oneWireSearch(address)){
                        gotoFirstLine();
                        for(char i = 0;i<4;i++){
                           sendInt(address[i]);
                           sendChar(' ');
                        }
                        gotoSecondLine();
                        for(char i = 4;i<8;i++){
                           sendInt(address[i]);
                           sendChar(' ');
                        }
                        
                    } else {
                        sendString("b�ad odczytu");
                    }
                }
	}
}
