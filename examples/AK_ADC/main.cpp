#include <msp430x14x.h>
#include "thermometer.h"
#include "wdt.h"
#include "lcd.h"


void main( void )
{
    wdtInit();                                    //inicjalizacja wdt

	initThermometer();							  //inicjalizacja termometru
	
    initLCD();                                    // inicjalizacja LCD
    clearDisplay();                               // czyszczenie LCD
	
    while(1){
		float temp = getTemperature();
		clearDisplay();
		sendFloat(temp, 1);
		//wait
    }
}
