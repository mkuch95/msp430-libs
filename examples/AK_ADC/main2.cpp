#include <msp430x14x.h>
#include "thermometer.h"
#include "wdt.h"
#include "lcd.h"


#define  INTERVAL  50000  //okres licznika 0,5s
#define  _5s       10     //okres pomiarów


void main( void )
{
    wdtInit();                                    //inicjalizacja wdt

	initThermometer();							  //inicjalizacja termometru
	
	
	//TIMER INIT
	CCR0 = INTERVAL;                   //ustala nowy okres licznika
	TACTL = TASSEL_2 | ID_3 | MC_1;        //źródło taktowania SMCLK, dzielone przez 8,tryb UP
	CCTL0 = CCIE;                      //uaktywnienie przerwania od TACCR0 CCIFG
	_BIS_SR(GIE);                    //właczenie przerwań
	//
	
    initLCD();                                    // inicjalizacja LCD
    clearDisplay();                               // czyszczenie LCD
	
    while(1){
		float temp = getTemperature();
		clearDisplay();
		sendFloat(temp, 1);
		
		
		_BIS_SR(LPM0_bits);               //wejście w tryb oszczędny
    }
}


#pragma vector=TIMERA0_VECTOR
__interrupt void Timer_A (void)
{
 if(++cntr==_5s)
 { 
  _BIC_SR_IRQ(LPM0_bits);            //wyjście z trybu oszczednego
  cntr=0;
  }
}
