#include <msp430x14x.h>
#include "thermometer.h"

void initThermometer(){
	ADC12CTL0 = ADC12ON | REFON | SHT0_15; //w�. rdzenia, w�.gen. nap. odniesienia, wyb�r nap. odniesienia
	ADC12CTL1 = SHP | CSTARTADD_0;       //pr�bkowanie impulsowe, wynik sk�adany w ADC12MEM0
	ADC12MCTL0 = INCH_10 | SREF_1;       //kana� 10, �r�d�o nap. odniesienia - wew. generator (1,5V)
	for(unsigned int k = 0; k < 0x3600; k++);           //czas na ustabilizowanie generatora nap. odniesienia
    ADC12CTL0 |= ENC;                 //uaktywnienie konwersji
}

float getTemperature(){
	ADC12CTL0 |= ADC12SC;             //start konwersji
	while((ADC12CTL1 & ADC12BUSY) == 1);  //czekanie na koniec konwersji
	return ADC12MEM0 * 0.10318 - 277.74647;
}
