#include <io430.h>
#include "interrupts.h"
#include "wdt.h"
#include "bdb.h"
#include "interrupts.h"


void wariat(){
    toggleStatus();
}


int main(){
  wdtInit();
  
  initAllBDB();
  
  setWDTHandler(wariat);
  startIntervalMode(I500ms);
  
  enableInterrupts();
  
  while(1){
    if(checkButtonPressed(B3))
      turnOnRel1();
    else
      turnOffRel1();
  }
  
}
