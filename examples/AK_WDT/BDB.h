
typedef enum buttons{
	B1,
	B2,
	B3,
	B4
} Buttons;

typedef enum devices{
	STATUS,
	REL1,
	REL2
} Devices;


//130 = 1000Hz
typedef enum sounds{
	LOW = 520,
	MEDIUM = 260,
	HIGH = 130
} Sounds;


void _delayms(int time);
void initDevices();
void initBuzzer();
void initButtons();
void initAllBDB();


void playSound(Sounds sound, int time);


int checkButtonPressedTime(Buttons button);

int checkButtonPressed(Buttons button);


void turnOn(Devices device);

void turnOnStatus();

void turnOnRel1();

void turnOnRel2();


void turnOff(Devices device);

void turnOffStatus();

void turnOffRel1();

void turnOffRel2();


void toggle(Devices device);

void toggleStatus();

void toggleRel1();

void toggleRel2();
