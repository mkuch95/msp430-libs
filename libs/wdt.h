
typedef enum timesInterval {
	I8us,		//ACLK:1 WDTIS:64
	I16us,		//ACLK:2 WDTIS:64
	I32us,		//ACLK:4 WDTIS:64
	I64us,		//ACLK:1 WDTIS:512
	I128us,		//ACLK:2 WDTIS:512
	I256us,		//ACLK:4 WDTIS:512
	I512us,		//ACLK:8 WDTIS:512
	I1024us,	//ACLK:1 WDTIS:8192
	I2048us,	//ACLK:2 WDTIS:8192
	I4096us,	//ACLK:1 WDTIS:32768
	I8192us,	//ACLK:2 WDTIS:32768
	I16384us,	//ACLK:4 WDTIS:32768
	I32768us,	//ACLK:8 WDTIS:32768
	
        //przybliżone wartości
        I1ms,          //I1024us       = ACLK:1 WDTIS:8192
        I10ms,         //I2048us * 5   = ACLK:2 WDTIS:8192  /5
        I50ms,         //I1024us * 49  = ACLK:1 WDTIS:8192  /49
        I100ms,        //I2048us * 49  = ACLK:2 WDTIS:8192  /49
        I500ms,        //I8192us * 61  = ACLK:2 WDTIS:32768 /61
        I1s,           //I16384us * 61 = ACLK:4 WDTIS:32768 /61
        I2s,           //I32768us * 61 = ACLK:8 WDTIS:32768 /61
} TimesInterval;


typedef enum timesWatchdog {
	W8us,		//ACLK:1 WDTIS:64
	W16us,		//ACLK:2 WDTIS:64
	W32us,		//ACLK:4 WDTIS:64
	W64us,		//ACLK:1 WDTIS:512
	W128us,		//ACLK:2 WDTIS:512
	W256us,		//ACLK:4 WDTIS:512
	W512us,		//ACLK:8 WDTIS:512
	W1024us,	//ACLK:1 WDTIS:8192
	W2048us,	//ACLK:2 WDTIS:8192
	W4096us,	//ACLK:1 WDTIS:32768
	W8192us,	//ACLK:2 WDTIS:32768
	W16384us,	//ACLK:4 WDTIS:32768
	W32768us	//ACLK:8 WDTIS:32768
} TimesWatchdog;


void wdtInit();

void setWDTHandler(void (*newWDTHandle)(void));

void startIntervalMode(TimesInterval time);

void startWatchdogMode(TimesWatchdog time);

void stopWDT();

void resetWDTCounter();
