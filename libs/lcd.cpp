#include <msp430x14x.h>
#include "lcd.h"
#include "delays.h"


//funkcje pomocnicze
void delayus(unsigned int time);
void delayms(unsigned char time);
void toggleE();
void setDataMode();
void setCmdMode();
void sendDataToLCD(unsigned char d);



//inicjalizacja LCD
void initLCD(){
    //ustawiamy porty dla LCD
    P2SEL &= ~(BIT2 | BIT3 | BIT4 | BIT5 | BIT6 | BIT7);
    P2OUT &= ~(BIT2 | BIT3 | BIT4 | BIT5 | BIT6 | BIT7);
    P2DIR |= (BIT2 | BIT3 | BIT4 | BIT5 | BIT6 | BIT7);
    //ustawiamy porty dla LCD
    
    delayms(100);                   //Delay 100ms
    
    sendCmd(0x33);
    delayms(10);
    sendCmd(0x32);
    
    sendCmd(DISP_ON);
    sendCmd(CLR_DISP);
    delayms(100);
}


//czyszczenie ekranu z jednoczesnym przesuni�ciem kursora na pozycje 0x0
void clearDisplay(){
    sendCmd(CLR_DISP);
    delayms(1);
}


//przej�cie do pierwszej linii
inline void gotoFirstLine(){
    sendCmd(DD_RAM_ADDR);
}


//przej�cie do drugiej linii
inline void gotoSecondLine(){
    sendCmd(DD_RAM_ADDR2);
}


//wysy�anie znaku do ekranu
void sendChar(unsigned char c){
    setDataMode();
    delayus(500);               //.5ms
    sendDataToLCD(c);
}


//wysy�anie komendy do ekranu
void sendCmd(unsigned char c){
    setCmdMode(); 
    delayms(1);                 //1ms
    sendDataToLCD(c);
}


//wysy�anie na ekran ci�gu znak�w
void sendString(char *str){
    while(*str){
        sendChar(*(str++));
    }
}


//wypisywanie na ekran liczby ca�kowitej
void sendInt(int number){
    if(number < 0){
        sendChar('-');
        number = -number;
    }
    
    if(number == 0){
        sendChar('0');
    } else {
        int tmp = number / 10;
        if(tmp)
            sendInt(tmp);
        
        sendChar('0' + number % 10);
    }
}


//wypisywanie na ekran liczby rzeczywistej z okre�lon� ilo�ci� cyfr po przecinku
void sendFloat(float number, char precision){
    if(number < 0){
        sendChar('-');
        number = -number;
    }

    //wy�wietlamy cz�� ca�kowit�
    sendInt(number);

    sendChar('.');
	
    for(char i = 0; i < precision; i++){
        number -= (int)number;//usuwamy cz�� ca�kowit�
        number *= 10;//przesuwamy znaki w lew� stron�
        
        sendChar('0' + (char)number);
    }
}


//tworzenie w�asnego znaku, funkcja przyjmuje jako argument tablic� dwuwymiarow� z punktami z jakich sk�ada si� znak, oraz z id znaku
void makeDefinedChar(char c[][5], DefinedChars charId){
    sendCmd(CG_RAM_ADDR + charId * 8);             //kursor ustawiany jest w odpowiednim miejscu w pami�ci
    
    for(char i = 0; i <= 7; i++)
    {
        char tmpChar = 0;
        
        for(char j = 0; j < 5; j++)
		tmpChar |= c[i][j] << (4 - j);
        
        sendChar(tmpChar);                        //wysy�anie do wy�wietlacza jednej linnii punkt�w
    }                                             //powtarzamy osiem razy
    
    sendCmd(CUR_HOME);                            //ustawianie kursora na pozycji startowej
}


//ustawianie kursora na dowolnej pozycji na ekranie
void setCursorPosition(unsigned char x, unsigned char y){
	//walidacja danych pozycji
	if(x > 0x27) return;
	if(y > 1) return;
	
	sendCmd(DD_RAM_ADDR + x + (y * 0x40));
}


//prze��czenie bitu E informuj�ce uk�ad aby odczyta� dane z port�w
void toggleE(){
    P2OUT |= BIT3;
    delayus(10);
    P2OUT &= ~BIT3;
}


//ustawianie trybu pracy: wysy�anie znak�w
inline void setDataMode(){
  P2OUT |= BIT2;
}


//ustawianie trybu pracy: wysy�anie komend
inline void setCmdMode(){
  P2OUT &= ~BIT2;
}



//wysy�anie danych do LCD, znak�w lub komend
void sendDataToLCD(unsigned char d){
    int temp;
                    
    temp = d & 0xf0;            //cztery starsze bity danych
    P2OUT &= 0x0f;
    P2OUT |= temp;
    toggleE();              
    temp = d & 0x0f;
    temp = temp << 4;           //cztery m�odsze bity danych
    P2OUT &= 0x0f;
    P2OUT |= temp;
    toggleE();
}
