#include <msp430x14x.h>
#include "interrupts.h"

void enableInterrupts(){
    _EINT();
}


void disableInterrupts(){
    _DINT();
}
