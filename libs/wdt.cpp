#include <msp430x14x.h>
#include "wdt.h"

void (*wdtHandle)(void);
unsigned int postScalerDivider = 1;
unsigned int postScalerValue = 1;


#pragma vector=WDT_VECTOR
__interrupt void wdt_handler(void){
	if(wdtHandle != 0){
		if(postScalerValue++ < postScalerDivider) return;
		postScalerValue = 1;
	    
	    wdtHandle();
    }
}


void wdtInit(){// na starcie timer wylaczony
  WDTCTL  = WDTPW + WDTHOLD;
}


void setWDTHandler(void (*newWDTHandle)(void)){
	wdtHandle = newWDTHandle;
}


void startIntervalMode(TimesInterval time){
	
	unsigned char wdtis = 0;
	postScalerValue = 1;
	postScalerDivider = 1;
	
	//ustawianie prescalera ACLK, oraz wielko�ci licznika WDT
	switch(time){
		case I8us:	//ACLK:1 WDTIS:64
			BCSCTL1 |= XTS;
			wdtis = WDTIS1 | WDTIS0;
			break;
		case I16us:	//ACLK:2 WDTIS:64
			BCSCTL1 |= XTS | DIVA0;
			wdtis = WDTIS1 | WDTIS0;
			break;
		case I32us:	//ACLK:4 WDTIS:64
			BCSCTL1 |= XTS | DIVA1;
			wdtis = WDTIS1 | WDTIS0;
			break;
		case I64us:	//ACLK:1 WDTIS:512
			BCSCTL1 |= XTS;
			wdtis = WDTIS1;
			break;
		case I128us:	//ACLK:2 WDTIS:512
			BCSCTL1 |= XTS | DIVA0;
			wdtis = WDTIS1;
			break;
		case I256us:	//ACLK:4 WDTIS:512
			BCSCTL1 |= XTS | DIVA1;
			wdtis = WDTIS1;
			break;
		case I512us:	//ACLK:8 WDTIS:512
			BCSCTL1 |= XTS | DIVA1 | DIVA0;
			wdtis = WDTIS1;
			break;
		case I1024us:	//ACLK:1 WDTIS:8192
                case I1ms:
			BCSCTL1 |= XTS;
			wdtis = WDTIS0;
			break;
		case I2048us:	//ACLK:2 WDTIS:8192
			BCSCTL1 |= XTS | DIVA0;
			wdtis = WDTIS0;
			break;
		case I4096us:	//ACLK:1 WDTIS:32768
			BCSCTL1 |= XTS;
			break;
		case I8192us:	//ACLK:2 WDTIS:32768
			BCSCTL1 |= XTS | DIVA0;
			break;
		case I16384us:	//ACLK:4 WDTIS:32768
			BCSCTL1 |= XTS | DIVA1;
			break;
		case I32768us:	//ACLK:8 WDTIS:32768
			BCSCTL1 |= XTS | DIVA1 | DIVA0;
			break;
                case I10ms:         //I2048us * 5   = ACLK:2 WDTIS:8192  /5
                        BCSCTL1 |= XTS | DIVA0;
			wdtis = WDTIS0;
                        postScalerDivider = 5;
                        break;
                case I50ms:         //I1024us * 49  = ACLK:1 WDTIS:8192  /49
                        BCSCTL1 |= XTS;
			wdtis = WDTIS0;
                        postScalerDivider = 49;
                        break;
                case I100ms:        //I2048us * 49  = ACLK:2 WDTIS:8192  /49
                        BCSCTL1 |= XTS | DIVA0;
			wdtis = WDTIS0;
                        postScalerDivider = 49;
                        break;
                case I500ms:        //I8192us * 61  = ACLK:2 WDTIS:32768 /61
                        BCSCTL1 |= XTS | DIVA0;
                        postScalerDivider = 61;
                        break;
                case I1s:           //I16384us * 61 = ACLK:4 WDTIS:32768 /61
                        BCSCTL1 |= XTS | DIVA1;
                        postScalerDivider = 61;
                        break;
                case I2s:           //I32768us * 61 = ACLK:8 WDTIS:32768 /61
                        BCSCTL1 |= XTS | DIVA1 | DIVA0;
                        postScalerDivider = 61;
                        break;
	}
	
	unsigned int i;
	do{
      IFG1 &= ~OFIFG;                           // czyszczenie flgi OSCFault
      for (i = 0xFF; i > 0; i--);               // odczekanie 
    }
    while ((IFG1 & OFIFG));
	
    WDTCTL = WDTPW | WDTCNTCL | WDTTMSEL | WDTSSEL | wdtis;
	
    IE1 |= WDTIE;
	
}


void startWatchdogMode(TimesWatchdog time){
	
	unsigned char wdtis = 0;
	
	//ustawianie prescalera ACLK, oraz wielko�ci licznika WDT
	switch(time){
		case W8us:	//ACLK:1 WDTIS:64
			BCSCTL1 |= XTS;
			wdtis = WDTIS1 | WDTIS0;
			break;
		case W16us:	//ACLK:2 WDTIS:64
			BCSCTL1 |= XTS | DIVA0;
			wdtis = WDTIS1 | WDTIS0;
			break;
		case W32us:	//ACLK:4 WDTIS:64
			BCSCTL1 |= XTS | DIVA1;
			wdtis = WDTIS1 | WDTIS0;
			break;
		case W64us:	//ACLK:1 WDTIS:512
			BCSCTL1 |= XTS;
			wdtis = WDTIS1;
			break;
		case W128us:	//ACLK:2 WDTIS:512
			BCSCTL1 |= XTS | DIVA0;
			wdtis = WDTIS1;
			break;
		case W256us:	//ACLK:4 WDTIS:512
			BCSCTL1 |= XTS | DIVA1;
			wdtis = WDTIS1;
			break;
		case W512us:	//ACLK:8 WDTIS:512
			BCSCTL1 |= XTS | DIVA1 | DIVA0;
			wdtis = WDTIS1;
			break;
		case W1024us:	//ACLK:1 WDTIS:8192
			BCSCTL1 |= XTS;
			wdtis = WDTIS0;
			break;
		case W2048us:	//ACLK:2 WDTIS:8192
			BCSCTL1 |= XTS | DIVA0;
			wdtis = WDTIS0;
			break;
		case W4096us:	//ACLK:1 WDTIS:32768
			BCSCTL1 |= XTS;
			break;
		case W8192us:	//ACLK:2 WDTIS:32768
			BCSCTL1 |= XTS | DIVA0;
			break;
		case W16384us:	//ACLK:4 WDTIS:32768
			BCSCTL1 |= XTS | DIVA1;
			break;
		case W32768us:	//ACLK:8 WDTIS:32768
			BCSCTL1 |= XTS | DIVA1 | DIVA0;
			break;
	}
	
	unsigned int i;
	do{
      IFG1 &= ~OFIFG;                           // czyszczenie flgi OSCFault
      for (i = 0xFF; i > 0; i--);               // odczekanie 
    }
    while ((IFG1 & OFIFG));
	
    WDTCTL = WDTPW | WDTCNTCL | WDTSSEL | wdtis;
	
}


void stopWDT(){
    WDTCTL = WDTPW + WDTHOLD;
}


void resetWDTCounter(){
	WDTCTL = WDTPW + WDTCNTCL;
}
