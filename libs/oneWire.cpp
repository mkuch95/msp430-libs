#include <msp430x14x.h>
#include "oneWire.h"


unsigned char ROM_NO[8]; //adres ROM ostatnio wykrytego uk�adu
unsigned char lastDiscrepancy; //miejsce od kt�rego szuka� kolejnych sprzeczno�ci
unsigned char lastFamilyDiscrepancy; //ostatnia sprzeczno�� w nazwie rodziny uk�adu
unsigned char lastDeviceFlag; //czy odnaleziono ostatni uk�ad


//funkcja odczekuj�ca 30us
void _delay30us(){
    _NOP();
    _NOP();
    _NOP();
    _NOP();
    _NOP();
    _NOP();
    _NOP();
}


//funkcja odczekuj�ca 60us
void _delay60us(){
    unsigned char time = 10;
    while(--time) _NOP();
}


//funkcja odczekuj�ca 480us
void _delay480us(){
    unsigned char time = 88;
    while(--time) _NOP();
}


//ustawienie na pinie stanu 1(pin jest wej�ciem, stan wysoki jest spowodowany podci�gni�ciem linni danych do zasilania poprzez rezystor)
#pragma inline=forced
inline void _setHighInput(){
    P1DIR &= ~BIT7;
}


//ustawianie na pinie stanu 0(zwieramy do masy)
#pragma inline=forced
inline void _setLowOutput(){
    P1DIR |= BIT7;
}


//sprawdzanie stanu pinu na wej�ciu
#pragma inline=forced
inline char _getInput(){
    return (P1IN & BIT7) >> 7;
}


//wys�anie bitu danych
#pragma inline=forced
inline void _writeBit(char val){
    if(val){
        _setLowOutput();
        _setHighInput();
        _delay60us();
    } else {
        _setLowOutput();
        _delay60us();
        _setHighInput();
    }
}


//odczyt bitu danych
char _readBit(){
    _setLowOutput();
    _setHighInput();
    
    _delay30us();
	
    return _getInput();
}


//inicjalizacja oneWire
void oneWireInit(){
    P1DIR &= ~BIT7;
    P1OUT &= ~BIT7;
}


//wysy�amy bajt danych
void oneWireWriteByte(char byte){
    for(char i = 0; i < 8; ++i){
        _writeBit(byte & (1 << i));
    }
}


//odczytujemy bajt danych
char oneWireReadByte(){
    char result = 0;
    
    for(char i = 0; i < 8; ++i){
        if(_readBit())
        result |= (1 << i);
        
        _delay60us();//~104 powinno by�
    }
    return result;
}


//pr�ba reseetu urz�dzenia, funkcja zwraca warto�� czy  urz�dzenia s� podpi�te
char oneWireReset(){
    _setLowOutput();
    
    _delay480us();
	
    _setHighInput();
    
    _delay60us();
    
    if(_getInput())//stan wysoki na wej�ciu, brak czujnika
		return 0;
    
    
    while(!_getInput());//czekamy a� stan linii osi�gnie stan wysoki
    
    return 1;
}


//resetowanie warto�ci wyszukiwania globalnych do stan�w pocz�tkowych
void oneWireSearchReset(){
    lastDiscrepancy = 0;
    lastFamilyDiscrepancy = 0;
    lastDeviceFlag = 0;
    for(char i = 0; i < 8; i++){
        ROM_NO[i] = 0;
    }
}


//wyszukiwanie adres�w ROM urz�dze� pod��czonych do sieci, zwraca warto�� czy pr�ba wyszukiwania adresu ROM si� powiod�a
char oneWireSearch(unsigned char *newAddr){
    unsigned char idBitNumber;
    unsigned char last_zero, romByteNumber, searchResult;
    unsigned char id_bit;
    
    unsigned char romByteMask, search_direction;
    
    // initialize for search
    idBitNumber = 1;
    last_zero = 0;
    romByteNumber = 0;
    romByteMask = 1;
    searchResult = 0;
    
    // if the last call was not the last one
    if(!lastDeviceFlag){
        // 1-Wire reset
        if(!oneWireReset()){
            lastDiscrepancy = 0;
            lastFamilyDiscrepancy = 0;
            lastDeviceFlag = 0;
            return 0;
        }
        
        // issue the search command
        oneWireWriteByte(0xF0);
        
        // loop to do the search
        do{
            id_bit = 0;
            // read a bit and its complement
            if(_readBit())
				id_bit = 0x2;
            
            if(_readBit())
				id_bit |= 0x1;
            
            // check for no devices on 1-wire
            if(id_bit == 3)
				break;
            
            // all devices coupled have 0 or 1
            if(id_bit > 0){
                search_direction = (id_bit >> 1);// bit write value for search
            } else {
                // if this discrepancy if before the Last Discrepancy
                // on a previous next then pick the same as last time
                if(idBitNumber < lastDiscrepancy)
					search_direction = ((ROM_NO[romByteNumber] & romByteMask) > 0);
                else
                // if equal to last pick 1, if not then pick 0
					search_direction = (idBitNumber == lastDiscrepancy);
                
                // if 0 was picked then record its position in LastZero
                if(search_direction == 0)
                {
                    last_zero = idBitNumber;
                    
                    // check for Last discrepancy in family
                    if(last_zero < 9)
                    lastFamilyDiscrepancy = last_zero;
                }
            }
            
            // set or clear the bit in the ROM byte romByteNumber
            // with mask romByteMask
            if(search_direction == 1)
				ROM_NO[romByteNumber] |= romByteMask;
            else
				ROM_NO[romByteNumber] &= ~romByteMask;
            
            // serial number search direction write bit
            
            _writeBit(search_direction);
            
            idBitNumber++;
            romByteMask <<= 1;
            
            // if the mask is 0 then go to new SerialNum byte romByteNumber and reset mask
            if(romByteMask == 0){
                romByteNumber++;
                romByteMask = 1;
            }
            
            
        }
        while(romByteNumber < 8);  // loop until through all ROM bytes 0-7
        
        // if the search was successful then
        if(!(idBitNumber < 65)){
            // search successful so set lastDiscrepancy,lastDeviceFlag,searchResult
            lastDiscrepancy = last_zero;
            
            // check for last device
            if(lastDiscrepancy == 0)
				lastDeviceFlag = 1;
            
            searchResult = 1;
        }
    }
    
    // if no device found then reset counters so next 'search' will be like a first
    if(!searchResult || !ROM_NO[0]){
        lastDiscrepancy = 0;
        lastFamilyDiscrepancy = 0;
        lastDeviceFlag = 0;
        searchResult = 0;
    }
    for (char i = 0; i < 8; i++)
    newAddr[i] = ROM_NO[i];
    
    return searchResult;
}

