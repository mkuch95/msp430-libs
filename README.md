**Architektura komputerów**  
Libraries for MSP430 microcontroller written in 2015 to Computer Architecture(sadly only the Polish comments)  

Libs:  
- BDB - allows to control Buttons, Diodes and Buzzer  
- lcd - basics operations with build lcd  
- oneWire - read the teperature from external sensor using 1wire protocol  
- thermometer - read temperature from internal sensor  
- uart - uart communication  
- wdt - watchdog control